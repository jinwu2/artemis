[tool.poetry]
name = "tft-artemis"
version = "0.0.21"
description = "Artemis is a machine provisioning service. Its goal is to provision a machine - using a set of preconfigured providers as backends - which would satisfy the given hardware and software requirements."
authors = [
  "Milos Prchlik <mprchlik@redhat.com>",
  "Anna Khaitovich <akhaitov@redhat.com>",
  "Evgeny Fedin <efedin@redhat.com>",
  "Miroslav Vadkerti <mvadkert@redhat.com>",
  "Ondrej Ptak <optak@redhat.com>",
  "Daniel Simko <dasimko@redhat.com",
  "Guy Inger <ginger@redhat.com>"
]
license = "Apache-2.0"
packages = [
  { include = "tft", from = "src" }
]
include = ["artemis/schema"]

[tool.poetry.dependencies]
python = "~3.7"
ansible-vault = "^1.2.0"
alembic = "^1.4.2"
awscli = "^1.16.298"
beaker-client = "^27.0"
beautifulsoup4 = "^4.6.3"
dramatiq = { version = "^1.7.0", extras = ["rabbitmq"] }
gluetool = "^1.24"
gunicorn = "19.9.0"
jinja2-ansible-filters = "^1.3.0"
jq = "^1.1.3"
molten = "^1.0.1"
Pint = "^0.17"
psycopg2 = "2.8.4"
python-openstackclient = "^5.0.0"
sqlalchemy = "^1.3.12"
stackprinter = "^0.2.4"
typing-extensions = "^3.7.4"
periodiq = "^0.12.1"
redis = "^3.5.3"
jsonschema = "^3.2.0"

[tool.poetry.dev-dependencies]
# All development dependencies are left unpinned, untill we run into troubles with a particular version.

# Install `toml` extra to allow parsing pyproject.toml - no need for special config file for coverage.
coverage = { version = "*", extras = ["toml"] }
darglint = "*"
flake8 = "*"
flake8-docstrings = "*"
flake8-use-fstring = "*"
isort = "*"
# This will be trimmed down to 0.730 or something because of much more specific mypy-extensions bellow. Blocked
# by Gluetool.
mypy = "*"
pytest = "*"
pre-commit = "*"
pytest-flake8 = "*"
pytest-icdiff = "*"
pytest-mock = "*"
redislite = "*"
sqlalchemy-stubs = "*"
sqlalchemy-utils = "*"

# mypy-extensions must match gluetool requiremens.
mypy-extensions = "0.4.1"

#
# Autogenerated docs
#
# NOTE FOR GLUETOOL DEVELS: gluetool must become more up-to-date and more tolerant when it comes to Sphinx versions,
# it is blocking us from using less buggy Sphinx with support for type annotations.
# Sphinx = "^3.2.1"
# sphinx-autodoc-typehints = "^1.11.0"
# sphinx-typlog-theme = "^0.8.0"

[tool.poetry.scripts]
artemis-api-server = "tft.artemis.api:main"
artemis-dispatcher = "tft.artemis.dispatcher:main"
artemis-init-sqlite-schema = "tft.artemis.db:init_sqlite"
artemis-init-postgres-schema = "tft.artemis.db:init_postgres"

[build-system]
requires = ["poetry>=0.12"]
build-backend = "poetry.masonry.api"

[tool.coverage.run]
source = [
  "src/tft/artemis"
]

[tool.isort]
py_version = "3"
line_length = "120"
multi_line_output = "2"
group_by_package = true

[tool.pytest.ini_options]
filterwarnings = [
  "ignore::DeprecationWarning"
]
