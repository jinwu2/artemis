import gluetool.glue
import gluetool.log
from gluetool.result import Result

from tft.artemis import Failure
from tft.artemis.drivers import PoolImageInfo
from tft.artemis.drivers.aws import AWSDriver
from tft.artemis.drivers.hooks import map_environment_to_image_info
from tft.artemis.environment import Environment


def hook_AWS_ENVIRONMENT_TO_IMAGE(
    *,
    logger: gluetool.log.ContextAdapter,
    pool: AWSDriver,
    environment: Environment,
) -> Result[PoolImageInfo, Failure]:
    return map_environment_to_image_info(
        logger,
        pool,
        environment,
        mapping_filename='artemis-image-map-aws.yaml'
    )
